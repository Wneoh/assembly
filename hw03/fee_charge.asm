;;This program basically asks user for the money that the user want it in cents then charge user for a 5% fees then sort it in quaters,nickels,dimes and pennies. 
;;
;; by WeiChin
;;
;; nasm -f elf fee_charge.asm
;; gcc fee_charge.asm
;; ./a.out
;;

%include "../lib/iomacros.asm"

        global main
        section .data

str_change:	db 	"Enter change (0-99):",0
str_service:	db 	"Service fees       :",0
str_remain:	db 	"Remaining change   :",0
str_quarter:	db	"Number of Quarters :",0
str_dimes:	db	"Number of Dimes    :",0
str_nickel:	db	"Number of Nickels  :",0
str_penny:	db	"Number of Pennies  :",0

new_line:	db 	10,0

	section .bss

left_over:	resd	1
remain:		resd 	1
fee:		resd	1
quarter:	resd	1
dimes:		resd	1
nickel:		resd 	1
penny:		resd	1

        section .text
main:
        put_str		str_change      ;;print out the string to stdout
        get_i		ebx             ;;get the user input and read it as int
	
	mov 		edx, 0		;; clear out edx -32bit
        imul		eax,ebx,5       ;; use imul to multiple and store it in eax -32bit
	mov		ecx,100		;; move 100 to ecx
	div 		ecx		;; divide eax with ecx which is input* 5/100 to get the (fees)
	
	sub		ebx,eax		;; sub take the input number subtract fees to get the (remain)
	
	mov 		[fee],eax
	mov		[remain],ebx

	xor		ecx,ecx		;;clear out everything	
	xor		edx,edx		;;cause it causes confusion
	xor 		eax,0
	xor		ebx,ebx

	mov		eax,[remain]	;; move remain to eax
	mov		ecx,25		;; move 25 into ecx
        div		ecx		;; remain/25 to get the number of quarters

	mov		[quarter],eax	;; move the number quarters into memory
	
	xor 		edx,edx		;;clear out 
	xor		eax,eax
	xor 		ecx,ecx

	mov		eax,[remain]	;; get the remain by subtracting 25* number of quarters	
	imul		edx,[quarter],25
	sub		eax,edx
	mov		[left_over],eax	;; to keep track of the left over, it changes everytime. 

	mov		edx,0		;; divide to get number of dimes 
	mov		ecx,10
	div		ecx		
	

	mov		[dimes],eax	;; move number of dimes into dimes memory 

	xor		eax,eax		;; clear out 
	imul		ebx,[dimes],10	;; get the number of dimes *10 
	sub		[left_over],ebx	;; then subtract it with the leftover after quarters
	
	xor 		ecx,ecx		;;clear out 
	xor		ebx,ebx
	

	mov		edx,0		;; move the remain to eax to divide, to find nickels
	mov		eax,[left_over]
	mov		ecx,5
	div		ecx

	mov		[nickel],eax	;; move the number of nickels into nickels

	mov		ecx,0		;; clear for multiplication
	xor		eax,eax		
	
	imul		ecx,[nickel],5	;; take the number of nickels *5
	sub		[left_over],ecx	;; then subract from it to get left_over
	
	xor		ecx,ecx		;; clearout
	
	mov 		edx,0		;; clearout for division	
	mov		eax,[left_over]	;; move the left_over into eax 
	mov		ecx,1		;; then 1 into ecx to divide 
	div		ecx		;; divide remain1/1
	
	mov		[penny],eax	;; move the number of penny into penny memory

	;;clearout	
	xor		eax,eax
	xor		ebx,ebx
	xor		edx,edx
	xor		ecx,ecx
			
	;; print out over here
        put_str		str_service	;; print out the string to stdout again
        put_i		[fee]	
	put_str		new_line
	
	put_str		str_remain
	put_i		[remain]
        put_str		new_line
      
	put_str		str_quarter
	put_i		[quarter]       
	put_str		new_line

	put_str		str_dimes
        put_i           [dimes]
        put_str         new_line
		
	put_str         str_nickel
        put_i           [nickel]
        put_str         new_line
	
	put_str		str_penny
        put_i           [penny]
        put_str         new_line

   	mov		eax,60
        xor		rdi,rdi
        syscall
