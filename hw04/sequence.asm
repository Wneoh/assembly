;;This program counts the length that of hailstone squence for the input number from 0 to 100,000
;;
;; by WeiChin
;;
;; nasm -f elf64 sequence.asm
;; gcc sequence.asm
;; ./a.out
;;

%include "../lib/iomacros.asm"

        global main
        section .data

ask_str:    	db	" has length ",0
end_line:       db      10,0

        section .bss
total_num 	resd 1;
number  	resd 1;
steps		resd 1;
answer		resd 1;
length   	resd 1;

        section .text

main:		mov     dword[number],2		;; set number as the number going to change throught the process
		mov 	dword[total_num],2	;; set total_number as the loop to go through


iloop:		mov	eax,[total_num]		
		mov	[number],eax
		mov 	dword[steps],0		;; reset steps once j loop is done, since I already got it print
		add	dword[total_num],1	;; incement i so that we can reach 100,000
		cmp	dword[total_num],100000
		jl	jloop			;; if it's less than 100,000, then go to jloop
		jg	end			;; if greater, then we are done 

jloop:		mov	eax,[number]		;; this cmp basically check if the number is 1, if yes then go print 
		cmp	eax,1
		je	double_check	
	
		mov 	edx,0			;; else just continue 
		mov	ecx,2
		div	ecx
	
		cmp	edx,0			;; div to find out if it's even odd by checking the remainder 
		jz	even			;; if ti's even then go even
		jg	odd			;; if it's odd then go odd


even:		mov	edx,0		
		mov	eax,[number]
		mov	ecx,2			
		div	ecx			;; take the number /2 
		mov	[number],eax		;; change the number to the result
		add	dword[steps],1		;; then add one to the steps
		jmp	jloop			;; go back jloop to continue

odd:		imul	eax,[number],3		;; times number with 3
		add	eax,1			;; add the result with 1
		mov	[number],eax		;; change the number to the result
		add	dword[steps],1		;; add 1 to the step
		jmp	jloop			;; go back jloop

print:		put_i	[total_num]		
		put_str ask_str
		put_i	[steps]	
		put_str	end_line
		jmp	iloop			;; go back to iloop

double_check:	cmp dword[steps],300		;; compare if the step is at or over 300 
		jge	print			;; if yes then print
		jmp	iloop			;; else skip

end:		mov	eax,60
		xor	rdi,rdi
        	syscall

