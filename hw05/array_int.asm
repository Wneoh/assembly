;
; array_int.asm
; 
; Set up a simple array of ints in memory and then play around with it.
;
; @author  Wei Chin
; @version 10/04/18
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main
                section .data
file:           db      "../data/numbers.txt",0
endl:           db      10,0
ave:		db	"Average: ",0
max_str:	db	"Maximum: ",0
min_str:	db	"Minimum: ",0

                section .bss
fp:             resq    1
total:		resd	1
total_num:	resd	1
min:		resd	1
max:		resd	1
array:          resd    1000000
                section .text
    
main:
		fopenr  [fp],file
                mov     ecx,array
		xor 	r8d,r8d
		xor 	r9d,r9d		

loop:           fget_i  [fp],ebx
                cmp     eax,-1
                je      close
                mov     [array+4*r8d],ebx
		inc 	r8d
		mov	[total_num],r8d
                jmp     loop


print:          cmp     r8d,0
                je      alldone
		dec	r8d
		cmp	r10d,[edx]
		jl	get_max
con1:		cmp	r11d,[edx]
		jg	get_min
con2:		add	r9d,[edx]
                put_i   [edx]
                put_str endl
                add     edx,4
                jmp     print

get_max:	mov	r10d,[edx]
		jmp	con2

get_min:	mov	r11d,[edx]
		jmp	con1	
	
close:          fclosem [fp]
		xor	edx,edx
                mov     edx,array
		mov	r10d,[edx]
		mov	r11d,[edx]
                jmp 	print

alldone:
		mov	eax,r9d
		;;put_i	eax
		;;put_str	endl
		xor	ecx,ecx
		mov	ecx,[total_num]
		;;put_i	ecx
		;;put_str	endl
		cdq	
		idiv	ecx
		put_str	ave
		put_i	eax
		put_str	endl
		put_str	min_str
		put_i	r11d
		put_str	endl
		put_str	max_str
		put_i	r10d
		put_str	endl

                mov     ebx,0           ; return 0
                mov     eax,1           ; on
                int     80h             ; exit
