; 
; array_str.asm
;
; Set up a simple array of ints in memory and then play around with it.
;
; @author  Wei Chin
; @version 10/04/18
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

                global main
                section .data
file:           db      "../data/names.txt",0
test:		db	"test",10
endl:           db      10,0

                section .bss
fp:             resq    1
array:          resb    10000*26
parallel_array:		resb	10000*26
                section .text

main:		fopenr  [fp],file
                mov     rcx,array
		mov	rbx,parallel_array
		xor 	r8d,r8d

loop:		fget_str [fp],rcx
		cmp	eax,-1
		je	close
		mov	[rbx],rax
		add	rbx,26
		add	rcx,26
		inc	r8d
		jmp	loop
	
	
close:		fclosem	[fp]
		xor	rdx,rdx
		xor	r11d,r11d
		mov	rdx,array
		mov	r11d,parallel_array
		jmp	traverse

traverse:	cmp	r8d,0
		je 	alldone
		cmp	dword[r11d],20
		jl	loop1
		put_str	rdx
		put_str	endl
		add	rdx,26
		add	r11d,26
		dec	r8d
		jmp	traverse
	
loop1:		add	r11d,26
		add	rdx,26
		dec	r8d
		jmp	traverse	



alldone:        mov     ebx,0           ; return 0
                mov     eax,1           ; on
                int     80h             ; exit

