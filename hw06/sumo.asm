; 
; sumo.asm
;
; A program that will tells the user the index of the tallest and the heaviest wrestle
;
; @author  Wei Chin
; @version 10/14/18
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define	RECSIZE 32
%define RANK	20
%define	HEIGHT	24
%define	WEIGHT	28

                global main
                section .data

file:           db      "../data/sumo.txt",0
tall_wrestle:	db	"The index of Tallest wrestle is ",0
text:		db	" which is ",0
heavy_wrestle:	db	"The index of Heavest wrestle is ",0
space:          db      " ",0
endl:           db      10,0

                section .bss

fp:             resq    1
array:          resb    100*RECSIZE
max_height:	resd	1
max_weight:	resd	1
height_name:	resq	1
weight_name:	resq	1
index_height:	resd	1
index_weight:	resd	1

                section .text

main:		;;mov	[rax+12],ecx
		;;put_str	[rax+12]
		;put_str	endl
		mov	rdx,array	
		fopenr	[fp],file
		xor	r8d,r8d
		xor	r11d,r11d
		xor	r12d,r12d
		xor	r13d,r13d
		xor	r14d,r14d
		xor	r15d,r15d
		mov	dword[max_height],0	; set up max as 0 to cmp
		mov	dword[max_weight],0

loop:           fget_i [fp],[rdx+RANK]		; read the elements of text into array
		fget_i [fp],[rdx+HEIGHT]
		fget_i [fp],[rdx+WEIGHT]
		fget_ch [fp],bl
		fget_str [fp],rdx
                cmp     eax,-1
                je	close
		add	rdx, 32			; jump 32 for the next set 
		inc	r8d			; counter to keep track how many elements 
                jmp     loop


close:          fclosem [fp]
                xor     rcx,rcx
                mov     rcx,array
                jmp     traverse

traverse:       cmp     r8d,0			; go through the array to find the index  
                je      print
		mov	r12d,[rcx+HEIGHT]
		cmp	r12d,[max_height]	; compare the a[i].weight with max_height
		jg	find_height
continue:	mov	r14d,[rcx+WEIGHT]
		cmp	r14d,[max_weight]	; compare a[i].weight with max_weight
		jg	find_weight
continue1:	add     rcx,32			; move 32bytes 
		dec	r8d
		inc	r11d	
                jmp     traverse

find_height:	mov	[max_height],r12d	; move the register into memory to avoid confusion 
		mov	[index_height],r11d		
		mov	[height_name],rcx
		jmp	continue

find_weight:	mov	[max_weight],r14d
		mov	[index_weight],r11d
		mov	[weight_name],rcx
		jmp	continue1

print:		put_str	tall_wrestle
		put_i	[index_height]
		put_str	text
		put_str	[height_name]	
		put_str	endl
		put_str heavy_wrestle
		mov	ebx,[index_weight]
		put_i	ebx
                put_i   [index_weight]
		put_str	text
		put_str	[weight_name]
                put_str endl


alldone:   	mov     ebx,0           ; return 0
                mov     eax,1           ; on
                int     80h             ; exit


