; 
; sumo.asm
;
; A program that tells the user the index,rank,name,height and weight of the tallest and the heaviest wrestle
;
; @author  Wei Chin
; @version 10/19/18
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define	RECSIZE 32
%define RANK	20
%define	HEIGHT	24
%define	WEIGHT	28

                global main
                section .data

file:           db      "../data/sumo.txt",0
tallest_str:    db      "The tallest man's height	:",0
rank:		db	"rank				:",0
weight_str:	db	"Weight				:",0
name_str:	db	"Name				:",0
index_str:	db	"Index				:",0
heaviest_str:   db      "The heaviest man's weight	:",0
height_str:     db      "Height				:",0
line:		db	"----------------------------------------------",0
endl:           db      10,0

                section .bss

fp:             resq    1
array:          resq    100
tallest:	resd	1
heaviest:	resd	1
index_tallest:	resd	1
index_heaviest:	resd	1
weight:		resd	1
height:		resd	1
rank_heaviest:	resd	1
rank_tallest:	resd	1
name_heaviest:	resb	20
name_tallest:	resb	20


                section .text

main:		fopenr [fp],file
		mov	r12,array
		
loop:		
		mov	rax,9
		mov	rdi,0
		mov	rsi,32
		mov	rdx,3
		mov	r10,0x22
		mov	r8,-1
		mov	r9,0
		syscall
		
		mov	rbx,rax

	        mov	[r12],rbx
		fget_i 	[fp],[rbx+RANK]		; read the elements of text into array
		fget_i 	[fp],[rbx+HEIGHT]
		fget_i 	[fp],[rbx+WEIGHT]
		fget_ch	[fp],cl
		fget_str[fp],rbx
                cmp     eax,-1
                je	close
		add	r12,8			; jump 8 btyes for the next array 
		inc	r14d			; counter to keep track how many elements 
                jmp     loop


close:          fclosem [fp]
		mov	r9,array
		mov	dword[tallest],0
		mov	dword[heaviest],0
		mov	rbx,0
		jmp	traverse

traverse:	cmp	r14d,0
		jle	print			; display when done
		mov	r10,[r9]		; mov each element into a specific register to compare
		mov	r13d,[r10+RANK]
		mov	r15d,[r10+HEIGHT]
		mov	r11d,[r10+WEIGHT]
		cmp	r15d,[tallest]
		jg	find_height		; find max height
continue:	cmp	r11d,[heaviest]
		jg	find_weight		; find max weight
continue1:	add	r9,8
		dec	r14d
		inc 	rbx
		jmp	traverse


find_height:	mov	[tallest],r15d		; mov [i].height into memory 
		mov	[index_tallest],rbx
		mov	[name_tallest],r10		
		mov	[weight],r11d
		mov	r8d,r11d
		mov	[rank_tallest],r13d
		jmp	continue		; jmp continue

find_weight:	mov	[heaviest],r11d		; mov [i].weight into memory
		mov	[index_heaviest],rbx
		mov	[name_heaviest],r10
		mov	[height],r15d
		mov	[rank_heaviest],r13d
		jmp 	continue1


print:   	put_str line
                put_str endl
		put_str	index_str
		put_i	[index_tallest]
		put_str	endl
		put_str	rank
		put_i	[rank_tallest]
		put_str	endl
		put_str name_str
                put_str [name_tallest]
                put_str endl
		put_str	tallest_str
		put_i	[tallest]
		put_str	endl
		put_str	weight_str
		put_i	r8d
		put_str	endl
		put_str	line
		put_str	endl
	
		;; start printing heaviest wrestle
		put_str index_str
                put_i   [index_heaviest]
                put_str endl
                put_str rank
                put_i   [rank_heaviest]
                put_str endl
                put_str name_str
                put_str [name_heaviest]
                put_str endl
                put_str heaviest_str
                put_i   [heaviest]
                put_str endl
                put_str height_str
                put_i   [height]
                put_str endl



alldone:	mov     ebx,0           ; return 0
                mov     eax,1           ; on
                int     80h             ; exit


