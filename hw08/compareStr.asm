;
; compareStr.asm
;
; A macro that compare two string to tell which string is bigger.
;

%macro  	cmp_str	2			;save context
		push	r8
		push	r9

		mov     r8,0		
%%loop:		mov	r9b,[%1+r8]
	 	cmp	r9b,[%2+r8]		;cmp first_str.charAt(i) with second_str.charAt(i)
		jne	%%done
		cmp	r9b,[%2+r8]		;cmp again to see if both are zero 
		jz	%%done
		inc	r8
		jmp	%%loop
	
%%done:		pop	r9
		pop 	r8
%endmacro

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

                global main

                section .data
endl:           db      10,0
done1_str:	db	"Str 1 is Bigger",0
done2_str:	db	"Str 2 is Bigger",0
done3_str:	db	"Both str are the same",0
str1:		db	"ABE",0
str2:		db	"LINCOLN",0


                section .text

main:
	mov	rbx,str1
	mov	rcx,str2
	
	
	cmp_str	rbx,rcx
	jg	done1
	jl	done2
	je	done3

done1:	put_str	done1_str
	put_str	endl
	jmp	done

done2:	put_str	done2_str
	put_str	endl
	jmp	done

done3:	put_str	done3_str
	put_str	endl

done:	mov     eax, 60                 ; system call 60 is exit
       	xor     rdi, rdi                ; exit code 0
        syscall







