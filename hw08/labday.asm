;
; labday.asm
;
; Playing around with the stack.
;

%macro	madd 2                     ; save context
        push    rcx
        push    rbx
	
	mov	ecx,%1
	mov	ebx,%2
      	add	ecx,ebx
	mov	%1,ecx               

       		                  ; restore context
        pop     rbx
        pop     rcx
%endmacro

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main

		section	.data
endl:		db	10,0

		section .bss
x:		resd	1
y:		resd	1

		section .text

main:
		;dump_regs
		;put_str	endl
		;push	rax
		;dump_regs
		;put_str	endl
		;push	rdi
		;dump_regs
		;put_str	endl
		;mov	rax,-1
		;mov	edi,-1
		;dump_regs
		;put_str	endl
		;pop	rax
		;pop	rdi
		;dump_regs
		
		mov	dword[x],2
		mov	dword[y],3
	
		xor	ebx,ebx
		xor	ecx,ecx
		mov	ebx,2
		mov	ecx,3	
		madd	[x],[y]
		
		madd 	ebx,ecx
			
		put_i	ebx
		put_i	[x]
		put_str	endl

		mov     eax, 60                 ; system call 60 is exit
		xor     rdi, rdi                ; exit code 0
		syscall
