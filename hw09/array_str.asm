; 
; array_str.asm
;
; Set up an array and use functions to play with it
;
; @author  Wei Chin
; @version 11/10/18
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

                extern readStrings      ; tells nasm that readStrings is defined externally

                global main
                section .data
file:           db      "../data/names.txt",0
file_str:	db	"The name of the input file used : ",0
index_str:	db	"The index of the longest name	: ",0
name_str:	db	"The longest name		: ",0
number_str:	db	"The name of the input file used : ",0
endl:           db      10,0

                section .bss
array:          resb    10000*27
total_Names:	resd	1
max:		resd	1
index_LongPos:	resd	1
name_LongPos:	resb	27

                section .text

main:		mov	dword[max],0
		mov	rdi,array		; move array to rdi as 1st parameter
		mov	rsi,file		; move file to rsi as 2nd paramter
		call	readStrings
		mov	[total_Names],rax	; mov total n into total_Names

		mov	rdi,array
		mov	rsi,rax			; set 2nd parameter to n
		
		call	findLongPos
		
		
		mov	[index_LongPos],edx	; mov register to memory  
		mov	[name_LongPos],r10
		put_str	number_str		; print statement starts here 
		put_i	[total_Names]
		put_str	endl
		put_str	index_str
		put_i	[index_LongPos]
		put_str	endl
		put_str	file_str
		put_str	file
                put_str endl
		put_str	name_str
		put_str	[name_LongPos]
		jmp	alldone



                section .text
strlen:		xor	rcx,rcx			; rcx as counter

loop_strlen:	cmp	dword[rdi+rcx],0	; find the lenght of the string
		je	done_strlen 
		inc	rcx
		jmp	loop_strlen

done_strlen:	mov	eax,ecx			; mov the length into eax
		ret



                section .text
findLongPos:	mov	r9d,0			; set r9d as index
		mov	rbx,0
		
loop_FindLong:	cmp	rsi,0
		je	done_FindLong
		call	strlen
		cmp	eax,ebx			; cmp rbx with current length 
		jg	foundMax		; if bigger then swap
continue:	add	rdi,27
		dec	rsi
		inc	r9d
		jmp	loop_FindLong

foundMax:	mov	ebx,eax			; set ebx as eax to find the max length
		mov	edx,r9d			; save the max length index and names 
		mov	r10,rdi
		jmp	continue
		
		
done_FindLong: ret	


alldone:        mov     ebx,0           ; return 0
                mov     eax,1           ; on
                int     80h             ; exit

