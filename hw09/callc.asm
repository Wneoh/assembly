;
; callc.asm
;
; Demonstrate calling a C-language function.
;
; @author  Terry Sergeant
; @version Fall 2016
;
; To complile this code:
;   gcc -c display.c               # produces display.o
;   nasm -f elf64 callc.asm        # produces callc.o
;   gcc callc.o display.o          # produces a.out

%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		extern findSuccPos	; tells nasm that displayNums is defined externally

		section .data
ints:		dd	68,-9,23,45,67,78,32,31,525,123 ; an array of 10 ints
n:		dd	10	; number of ints in the array
endl:		db	10,0

section .text

		global 		main
main:
		;mov	rax,-1
		;mov	rbx,-1
		;mov	rcx,-1
		;mov	rdx,-1
		;mov	rsi,-1
		;mov	rdi,-1
		;mov	r8,-1
		;mov	r9,-1
		;mov	r10,-1
		;mov	r11,-1
		;mov	r12,-1
		;mov	r13,-1
		;mov	r14,-1
		;mov	r15,-1
		;dump_regs

		mov	rdi,ints	; rdi= param 1 which is address of array
		xor	rsi,rsi		; rsi= second params which is num of array elems
		mov	esi,[n]
		xor	rdx,rdx
		mov	edx,40
		call	findSuccPos	; call externally defined displayNums
		put_i	eax
		put_str	endl
		;dump_regs


alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit

