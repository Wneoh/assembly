;
; Use double precison float register to find the estimate for pi.
;
; @author  Wei Chin
; @version 11/20/18
;
%include "../lib/iomacros.asm"

extern fabs

		section .data
goal:		dq	3.141592653589793       ; PI
one:		dq	1.0			; one
three:		dq	3.0
one_two:	dq	1.5
n:		dd	0
two:		dq	2.0
zero:		dq	0.0
endl:		db	10,0
cutoff:		dq	0.0000001
estimate_str:	db	"Estimation for pi:",0


		section .bss

estimate:       resq    1



		section .text

		global 		main
main:
		movsd	xmm8,[cutoff] 	

		mov	rdi,[n]
main_loop:	call	estimate_pi	

		movsd	[estimate],xmm10
		subsd	xmm10,[goal]		; estimate - goal
		movsd	xmm0,xmm10		
		call 	fabs			; |error|
		ucomisd	xmm0,[cutoff]		; compare error with cutoff
		jbe	alldone
		put_str estimate_str
                put_dbl [estimate]
		put_str	endl	
		add	dword[n],1
		mov	rdi,[n]			; inc n
		jmp	main_loop
		jmp alldone	





estimate_pi:	mov     rbx,1
                mov     rdx,1
		mov	r8,1
		mov	r9,rdi
		movsd	xmm10,[one]
		movsd	xmm6,[one]
		movsd	xmm7,[three]
		movsd	xmm5,xmm10
		movsd   xmm11,[one]
                movsd   xmm12,[two]
		movsd	xmm14,[one]
		movsd	xmm15,[three]
			
loop:		cmp	r9,0
		jle	case_zero	
		cmp	r9,1
		jge	function

                

continue:	mulsd	xmm10,xmm6	
		cmp	r8,rdx
		je	special

		addsd	xmm10,[one]	
		
continue1:	dec	r9
		jmp	loop	


case_one:	movsd	xmm6,[one]
		movsd	xmm7,[three]
		divsd	xmm6,xmm7
		mulsd	xmm10,xmm6
		inc	rbx
		jmp 	continue1

case_zero:	cmp	rbx,r8
		je	done
		put_i	ebx
		put_i	r8d
		put_str	endl
		addsd	xmm10,[one]
		jmp	done

function:
		mov		rcx,r9
		dec		rcx
		cvtsi2sd	xmm13,rcx		; n-1
		
	
		movsd		xmm11,[one]		; set back original value
		movsd		xmm12,[two]
		movsd		xmm14,[one]
		movsd		xmm15,[three]
		
		
		mulsd		xmm11,xmm13		; (n-1)*1
		mulsd		xmm12,xmm13		; (n-1)*2


		addsd		xmm14,xmm11		; add nominator  (n-1)*1+1
		addsd		xmm15,xmm12		; add denominator (n-1)*2+3

		movsd		xmm6,xmm14		; mov answer into xmm6
		movsd		xmm7,xmm15		; mov answer into xmm7
		
		divsd		xmm6,xmm7
		cmp     	rbx,rdx
                je      	first_case


		jmp 		continue


special:	inc		r8
		jmp 		continue1

first_case:	addsd		xmm6,[one]		; case that only happenes once
		inc		rbx
		jmp 		continue

done:
		mulsd		xmm10,[two]		; times everything with 2
		ret		
		

alldone: 
		mov		ebx,0		; return 0
		mov		eax,1		; on
		int		80h		; exit

