;
; Builds nerd cards and uses bit operations to perform various actions.
;
; To compile:
; gcc -c readcodes.c
; nasm -f elf64 nerdcard.asm
; gcc nerdcard.o readcodes.o
; ./a.out
;
; A code record looks like this:
;
; Code {
;	int id;
;	int score;
;	char description[256];
; };
;
;
; A nerd record will look like this:
;
; Nerd {
;	char name[80];
;	long code;
; };
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"
%define ID 0
%define SCORE 4
%define DESCRIPTION 8
%define	NAME 0
%define BITSTRING 80 

		extern readCodes
		section .data
codefile:	db	"codes.txt",0
file:		db	"nerds.txt",0
space:		db	" ",0
everyone_str:	db	"Achievements Everyone Has ...",0
nobody_str:	db	"Achievements Nobody Has ...",0
dash:		db	"---------------------------------------------------",0	
endl		db	10,0


		section .bss
fp:		resq	1
codes:		resq	64		; up to 64 codes are supported
coden:		resq	1		; number of nerds we have
nerds:		resd	1		; total nerds 
array:		resb	88*8
bitstring:	resq	1
		section .text
		global 		main
main:
	
		; nerds = readNerds(filename,array)
		mov	rdi,file
		mov	rsi,array
		call	readNerds
		mov	[nerds],eax

		; displayNerds(nerds,array)
		mov	rdi,rax
		mov	rsi,array
		call	displayNerds	
		
		; coden= readCodes(codes,codefile)
		mov	rdi,codes
		mov	rsi,codefile
		call	readCodes
		mov	[coden],rax

		; doneByAll(nerds,codes,array)
		mov	eax,[nerds] 
                mov     rdi,rax
		mov	rsi,codes
		mov	rdx,array
                call    doneByAll

		; doneByNone(nerds,codes,array)
	
		mov	eax,[nerds]	
		mov	rdi,rax		; total nerds
		mov	rsi,codes	; array of codes
		mov	rdx,array	; array of nerds
		call 	doneByNone

theend:
                mov     eax, 60
                xor     rdi, rdi
                syscall


		; displayCodes(codes,n)
		;mov	rdi,codes
		;mov	rsi,[coden]
		;call	displayCodes

;--------------------------------------------------------------------------------
; readNerds(filename,array)
;---------------------------
; read nerds file, create a nerdcard object and add them to array 
;

readNerds:
		xor	rdx,rdx
		xor	rcx,rcx
		fopenr  [fp],rdi

loop_name:      fget_str [fp],rsi         ; read the elements of text into array
		cmp	eax,-1
		je	done_readNerds
		jmp	num


num:  		xor	r15,r15
loop_num:	fget_i 	[fp],ecx	;read into a temp register 
		cmp     eax,-1
		je	continue
		mov	edi,ecx
		call	add_set
                jmp 	loop_num
		

continue:	inc 	rdx			 ; counter to keep track how many elements
		mov	[rsi+BITSTRING],r15
		add	rsi,88
		jmp	loop_name	
		

add_set:	mov	r8,1
		mov	ecx,edi
		shl	r8,cl
		or	r15,r8
		ret


done_readNerds:	mov	rax,rdx			; return total num of elem in eax
		ret



;--------------------------------------------------------------------------------
; displayNerds(nerds,array)
;---------------------------
; display nerds along with id numbers
;

displayNerds:	xor	r8,r8		

loop_i:		cmp	rdi,r8			; counter to stop
		je	done_displayNerds
		put_i	r8d
		put_str	space
		put_str	rsi
		put_str	space
		put_i	[esi+BITSTRING]
		put_str	endl
		add	rsi,88
		inc	r8
		jmp	loop_i

done_displayNerds:	ret


;--------------------------------------------------------------------------------
; doneByAll(nerds,codes,array)
;---------------------------
; display achivement that done by all nerds 
;

doneByAll:
               	
		put_str	endl
		put_str	dash
		put_str	endl
		put_str	everyone_str
		put_str	endl
		put_str	dash
		put_str	endl

		mov     rbx,0xFFFFFFFFFFFFFFFF		; use this register as a bit masking 
		xor	r10,r10
               	xor     r11,r11
		xor	rcx,rcx
		xor	r12,r12	
		xor	r13,r13
		xor	r14,r14

loop_doneByAll:
                cmp     r12, rdi
                jge     print

                mov     r11, [rdx+BITSTRING]			; continuously and with the next bit string 	
                and     rbx, r11				; the result will be left with the achivement that everyone has

                inc     r12
                add     rdx,88
                jmp     loop_doneByAll

print:		xor	rcx,rcx
loop_print:
                cmp    rcx, 61
                je     done_doneByAll

                mov     r10, 1
                shl     r10, cl
                and     r10, rbx
                cmp     r10, 0
                je      caseNotMatch

                mov     r13, [rsi+8*rcx]
                put_i   [r13+ID]
                put_str space
                add     r13, DESCRIPTION
                put_str r13
                put_str endl

caseNotMatch:
                inc     rcx
                jmp     loop_print

done_doneByAll:	ret


;-------------------------------------------------------------------------------
; doneByNone(nerds,codes,array)
;---------------------------
; display codes alongwith id numbers achieved
;

doneByNone:

                put_str endl
                put_str dash
                put_str endl
                put_str nobody_str
                put_str endl
                put_str dash
                put_str endl

                mov     rbx, 0		                ; use this register as a bit masking
                xor     r10,r10
                xor     r11,r11
                xor     rcx,rcx
                xor     r12,r12
                xor     r13,r13
		xor	r14,r14

loop_doneByNone:
                cmp     r12,rdi
                jge     print1

                mov     r11, [rdx+BITSTRING]
                or      rbx, r11

                inc     r12
                add     rdx,88
                jmp     loop_doneByNone

print1:		xor	rcx,rcx
		not	rbx				; except I not the result to get the opposite
loop_print1:
                cmp    	rcx, 61
                je     	done_doneByNone

                mov     r10, 1
                shl     r10, cl
                and     r10, rbx
                cmp     r10, 0
                je      caseNotMatch1

                mov     r13, [rsi+8*rcx]
                put_i   [r13+ID]
                put_str space
                add     r13, DESCRIPTION
                put_str r13
                put_str endl

caseNotMatch1:
                inc     rcx
                jmp     loop_print1

done_doneByNone: ret





;-------------------------------------------------------------------------------
; displayCodes(codes,n,array)
;---------------------------
; display codes along with id numbers
;
displayCodes:
		xor	rcx,rcx

displayCodesLoop:
		cmp	rcx,rsi
		jge	endDisplayCodes
		mov	r8,[rdi+8*rcx]
		put_i	[r8+ID]
		put_ch	[space]
		put_i	[r8+SCORE]
		put_ch	[space]
		add	r8,DESCRIPTION
		put_str	r8
		put_str	endl
		inc	rcx
		jmp	displayCodesLoop

endDisplayCodes:
		mov	rax,rcx
		ret
