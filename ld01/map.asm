;;
;;
;;
;;
;;

%include "../lib/dumpregs.asm"
	
	global main	
	section .data

int:	dd	68,-9
floaty:	dd	-68.125
char:	dd	"Greetings!",10,0
bighex:	dq	10h
mediumbin: dw	10b
nightnight:	db	"ZZZZZZZZ"

	section .bss
somememory: resd 2

	section .text
main:

		
	mov qword[somememory],0x33333333

	mov rax,[int]
	mov rbx,[int+8]
	mov rcx,[int+16]
	mov rdx,[int+24]
	mov rsi,[int+32]
	mov rdi,[int+40]
	mov r8,[int+48]
	
	dump_regs


	mov     eax, 60                 ; exit 0
	xor     rdi, rdi
	syscall


