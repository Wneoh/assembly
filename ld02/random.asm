;; This program basically asking user for input which is the number that computer already had in mind. It's a guessing game.
;;
;; by WeiChin
;;
;; nasm -f elf random.asm
;; gcc random.asm
;; ./a.out
;;

%include "../lib/iomacros.asm"

        global main
        section .data

ask:	db	"Guess my number bro :(1-100)",0
correct_str:	db	"Congratz Bruh, You got it!",0
correct1_str:	db	"The correct num:",0
lesser_str:	db	"Try it again, You number is too low!",0
greater_str:	db	"Try it again, Your number is too high!",0
end_line:	db	10,0

	section .bss
answer	resd 1;
guess	resd 1;	

	section .text
main:
	add 	eax,ebx
	add 	eax,ecx
	add 	eax,edx
	add	eax,esi
	add	eax,ebp
	add	eax,esi
	add	eax,edi
	add	eax,esp
	add	eax,r8d
	add	eax,r9d
	add	eax,r10d
	add	eax,r11d
	add	eax,r12d
	add	eax,r13d
	add	eax,r14d
	add	eax,r15d
	mov 	ebx,100
	mov	edx,0
	div 	ebx
	add 	edx,1
	mov 	[answer],edx 

	put_str	ask
	get_i	[guess]
	
again:		cmp	dword[guess],edx
		je	correct
		jg	greater
		jl	lesser


correct:	put_str	correct_str
		put_str	correct1_str
		put_i	[answer]
		put_str	end_line
		mov	eax,60
		xor	rdi,rdi
		syscall

lesser:		put_str	lesser_str
		put_str	end_line
		get_i	[guess]
		jmp again

greater:	put_str	 greater_str
		put_str	end_line
		get_i	[guess]
		jmp again

