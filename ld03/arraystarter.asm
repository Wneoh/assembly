;
; arraystarter.asm
;
; Set up a simple array of ints in memory and then play around with it.
;
; @author  Terry Sergeant
; @version Fall 2016
;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		section .data
myarray:	dd	0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xAA
file:		db 	"numbers.txt",0
endl:		db	10,0

		section .bss
fp:		resq	1
array:		resd	100
		section .text
		global 		main
main:
		; before running this guess what the output will be ... THEN run it.
		;put_i	[myarray]
		;put_str	endl

		; write the necessary statements, without using a loop, to display all elements of myarray
		;xor	rax,rax
		;mov	rax,myarray
		;put_i	[rax]
		;put_str	endl
		;add	rax,4
		;put_i	[rax]
		;put_str	endl
		;add	rax,4
		;put_i	[rax]
		;put_str	endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
                ;add     rax,4
                ;put_i   [rax]
                ;put_str endl
        	
		;xor rax,rax
		; now display myarray using a loop

		;xor eax,eax
		;cmp eax,10
		;jge alldone
		;put_i [myarray+4*eax]
		;put_str	endl
		;inc eax
		;jmp loop



		; bring a printout of your completed source code to labday

		
		
		fopenr	[fp],file
		mov	ecx,0
		mov	rcx,array
 
loop:		fget_i	[fp],ebx
		cmp	eax,-1
		je	close
		mov	[rcx],ebx
		add	rcx,4
		jmp	loop

		
print:		xor	ebx,ebx
		cmp	dword[rdx],0
		je	alldone
		put_i	[rdx]
		put_str	endl
		add 	rdx,4
		jmp 	print

close:		fclosem [fp]
		mov	rdx,array
		jmp print
		

alldone: 	
		mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit

